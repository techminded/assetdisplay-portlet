#AssetDisplay Portlet

portlet that takes custom field name and it's value as parameters and display's journal article content for given value and current locale

To have rewrtie where yoursite.com/1235/ is rewriten to the page with layout on url news and assetdisplay portlet on it that will show webcontent with value 12345 for custom field named **externalId** and **Integer** type you should perform the wollowin tweaks:

1. Add news layout to site with assetdisplay portlet on it

2. Use frontend web-server as apache or nginx, for apache enable **proxy** and **proxy_http** modules and add the following lines to your portal's virtual host configuration:
```
    ProxyPreserveHost on
    ProxyPass / http://localhost:8080/
```
3. Put the following rules to file: tomcat-7.0.42/webapps/ROOT/WEB-INF/urlrewrite.xml
```
<urlrewrite>
...
        <rule>
                <from>^/(\d+)/</from>
                <to type="forward">/web/guest/news?fieldName=externalId&amp;fieldValue=$1</to>
        </rule>
..
</urlrewrite>
```
beaware \d+ in regex will match only number values, change it to .* to match all symbols or any other regular experssion mask and use string typed custom field
 
4. restart portal after reconfigured