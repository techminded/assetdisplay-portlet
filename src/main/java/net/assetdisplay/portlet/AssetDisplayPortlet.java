package net.assetdisplay.portlet;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.model.ExpandoValueModel;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalArticleDisplay;
import com.liferay.portlet.journal.model.JournalArticleResource;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journal.service.JournalArticleResourceLocalServiceUtil;
import com.liferay.portlet.journalcontent.util.JournalContentUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import net.assetdisplay.util.AssetDisplayUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class AssetDisplayPortlet extends MVCPortlet {

    public static final String FIELD_NAME = "fieldName";
    public static final String FIELD_VALUE = "fieldValue";
    public static final String FIELD_KEYWORDS = "keywords";
    public static final String FIELD_DESCRIPTION = "descripton";

    private static final Log _log = LogFactoryUtil.getLog(AssetDisplayPortlet.class);

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
            throws PortletException, IOException {

        HttpServletRequest request = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));

        String fieldValue = request.getParameter(FIELD_VALUE);
        String fieldName = request.getParameter(FIELD_NAME);

        ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);


        try {

            JournalArticleDisplay display = AssetDisplayUtil.getArticleByField(themeDisplay, fieldName, fieldValue);

            renderRequest.setAttribute("title", display.getTitle());
            renderRequest.setAttribute("text", display.getContent());

            setMetaPlaceholderValues(renderRequest);

        } catch (Exception e) {
            _log.error("error fetching content by field name ", e);
        }
        super.doView(renderRequest, renderResponse);
    }
    public void setMetaPlaceholderValues(RenderRequest renderRequest) {

        ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

        HttpServletRequest request = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));

        HttpSession session = request.getSession();
        // :TODO replace with fetch by article id and field name but not value
        ExpandoValue keywordsValue = AssetDisplayUtil.getFieldValue(themeDisplay, FIELD_KEYWORDS, null);
        if (keywordsValue != null) {
            session.setAttribute(WebKeys.PAGE_KEYWORDS, keywordsValue.getData());
        }

        ExpandoValue descriptionValue = AssetDisplayUtil.getFieldValue(themeDisplay, FIELD_DESCRIPTION, null);
        if (descriptionValue != null) {
            session.setAttribute(WebKeys.PAGE_DESCRIPTION, descriptionValue.getData());
        }
    }
}
