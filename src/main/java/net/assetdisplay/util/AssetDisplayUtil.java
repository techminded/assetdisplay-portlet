package net.assetdisplay.util;


import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalArticleDisplay;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journalcontent.util.JournalContentUtil;

import java.util.List;

public class AssetDisplayUtil {

    private static final Log _log = LogFactoryUtil.getLog(AssetDisplayUtil.class);

    public static JournalArticleDisplay getJournalArticleDisplay(ThemeDisplay themeDisplay, Long entryId) {
        JournalArticle article = null;
        JournalArticleDisplay display = null;
        try {
            article = JournalArticleLocalServiceUtil.getArticle(entryId);
            String languageId = themeDisplay.getLanguageId();
            display = JournalContentUtil.getDisplay(article.getGroupId(), article.getArticleId(), null, languageId, themeDisplay);
        } catch (Exception e) {
            _log.error("error fetching journal article display", e);
        }
        return display;
    }

    public static DynamicQuery queryExpandoValueByField(long companyId, long classNameId, String fieldName, String fieldValue) {
        DynamicQuery dynamicQuery = null;
        try {
            ExpandoTable customFieldsTable = ExpandoTableLocalServiceUtil.getTable(companyId, classNameId, ExpandoTableConstants.DEFAULT_TABLE_NAME);
            ExpandoColumn column = ExpandoColumnLocalServiceUtil.getColumn(companyId, classNameId, ExpandoTableConstants.DEFAULT_TABLE_NAME, fieldName);

            dynamicQuery = DynamicQueryFactoryUtil.forClass(ExpandoValue.class, PortalClassLoaderUtil.getClassLoader());
            dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
            dynamicQuery.add(RestrictionsFactoryUtil.eq("classNameId", classNameId));
            dynamicQuery.add(RestrictionsFactoryUtil.eq("tableId", customFieldsTable.getTableId()));
            dynamicQuery.add(RestrictionsFactoryUtil.eq("columnId", column.getColumnId()));
            dynamicQuery.add(RestrictionsFactoryUtil.eq("data", fieldValue));
        } catch (Exception e) {
            _log.error("error querying expando by field", e);
        }
        return dynamicQuery;
    }

    public static JournalArticleDisplay getArticleByField(ThemeDisplay themeDisplay, String fieldName, String fieldValue) {
        Long companyId = themeDisplay.getCompanyId();
        Long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class.getName());
        DynamicQuery dynamicQuery = AssetDisplayUtil.queryExpandoValueByField(companyId, classNameId, fieldName, fieldValue);
        JournalArticleDisplay display = null;
        try {
            List<ExpandoValue> result = ExpandoValueLocalServiceUtil.dynamicQuery(dynamicQuery);
            Long entryId = result.get(0).getClassPK();
            display = AssetDisplayUtil.getJournalArticleDisplay(themeDisplay, entryId);
        } catch (Exception e) {
            _log.error("error querying expando by field", e);
        }
        return display;
    }

    public static ExpandoValue getFieldValue(ThemeDisplay themeDisplay, String fieldName, String fieldValue) {
        Long companyId = themeDisplay.getCompanyId();
        Long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class.getName());
        DynamicQuery dynamicQuery = queryExpandoValueByField(companyId, classNameId, fieldName, fieldValue);

        try {
            List<ExpandoValue> result = ExpandoValueLocalServiceUtil.dynamicQuery(dynamicQuery);
            return result.get(0);

        } catch (Exception e) {
            _log.error("error querying expando value by field", e);
        }
        return null;
    }

}
